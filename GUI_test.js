#!/usr/bin/env node

 /** Preliminary version of a GUI testing tool using the socket API of ownCloud's 
 * Desktop Client.
 * 
 * @author Dominik Schmidt <dev@dominik-schmidt.de>
 */

var sleep = require('sleep');
var s = require('net').Socket();
var rsvp = require('rsvp');
var exec = require('child_process').exec;

global.screenshotNo = 0; 

global.takeScreenshot = function(value) {
    global.screenshotNo += 1;
    var screenshot   = 'import -window root screenshot' + global.screenshotNo + '.png';
    exec(screenshot);
    return value;
}


// general helper to send a \n terminated message to `s`
global.send = function (message)
{
    console.log('SEND: ', message)
    s.write(message + '\n');
}

// async handling
var counter = 0;
var promises = {};
global.invokeCommand = function(command, args) {
    send(command+':'+counter+'|'+JSON.stringify(args))
    
    var promise = new rsvp.Promise(function(resolve, reject) {
        promises[counter] = { resolve: resolve, reject: reject };
    });
    
    counter++;
    
    return promise;
}

// placeholder test framework
var tests = [];
runTests = async function() {
    console.log('Start '+tests.length +' tests...');
    var successfulTests = 0;
    var failedTests = 0;
    var finishedTests = 0;

    tests.forEach(test => {
        var testEnv = {
            assertEqual: function(left, right) {
                if(left != right) {
                    throw new Error("ASSERTION FAILED: " + left + ' : ' + right);
                }
            }
        };
    
        test.func(testEnv).then(() => {
            console.log('*** ' + test.name + ': succeeded');
            successfulTests++;
        }, () => {
            console.log('*** ' + test.name + ': failed');
            failedTests++;
        }).then(() => {
            finishedTests++;
            if(finishedTests == tests.length) {
                process.exit(finishedTests === successfulTests ? 0 : 1);
            }   
        });
    });
    
    setTimeout(() => {
        if(finishedTests != tests.length) {
            console.error('Timed out');
            process.exit(1);
        }
    }, 50000);
}

test = async function(name, func) {
    tests.push({name:name, func:func});
}

s.connect('/tmp/runtime-root/ownCloud/socket');
s.on('connect', async () => {
    runTests();
});


var dataLineHandler= function(line) {
//     console.log('data:', line);
    var split = line.split('|');
    if(split[0]=='RESOLVE') {
        promises[split[1]].resolve(split[2].trim());
    } else if(split[0]=='REJECT'){
        promises[split[1]].reject(split[2].trim());
    }
}

s.on('data', function(d){
    var data = d.toString();
    console.log("DATA: ", data);
    data.split('\n').forEach((line) => {
        dataLineHandler(line);
    })
});

s.on('close', function() {
   console.log("closed"); 
});

rsvp.on('error', function(reason) {
  console.assert(false, reason);
});

// new async helper
global.getValue = function(objectName, property) {
    return invokeCommand('ASYNC_GET_WIDGET_PROPERTY', {objectName: '_accountToolbox', property: 'text'});
}

global.setValue = function(objectName, propertyName, value)
{
    return invokeCommand('ASYNC_SET_WIDGET_PROPERTY', {objectName: objectName, property: propertyName, value: value}).then(takeScreenshot);
}

global.invokeMethod = function(objectName, method) {
    return invokeCommand('ASYNC_INVOKE_WIDGET_METHOD', {objectName: objectName, method: method}).then(takeScreenshot);
}

global.waitForSignal = function(objectName, signalSignature) {
    return invokeCommand('ASYNC_WAIT_FOR_WIDGET_SIGNAL', {objectName: objectName, signalSignature: signalSignature});
};

global.listObjects = function() {
    return invokeCommand('ASYNC_LIST_WIDGETS', {});
};

global.triggerMenuAction = function(objectName, actionName) {
    return invokeCommand('ASYNC_TRIGGER_MENU_ACTION', {objectName: objectName, actionName: actionName}).then(takeScreenshot);
};

test('check first button value', async t => {
    t.assertEqual(await getValue('_accountToolbox', 'text'), 'Account');
});

test('check second button value', async t => {
    t.assertEqual(await getValue('_accountToolbox', 'text'), 'Account2');
});

test('add demo account', async t => {
    // Wizard is launched by default, no need to trigger the menu action:
    //triggerMenuAction('_accountToolbox', 'addAccountAction');
    await setValue('leUrl', 'text', 'demo.owncloud.com');
    await invokeMethod('__qt__passive_wizardbutton1', 'click');
    await setValue('leUsername', 'text', 'demo');
    await setValue('lePassword', 'text', 'demo');
    await waitForSignal('owncloudWizard', 'currentIdChanged(int)');
    await invokeMethod('__qt__passive_wizardbutton1', 'click');
    await waitForSignal('owncloudWizard', 'currentIdChanged(int)');
    await invokeMethod('__qt__passive_wizardbutton6', 'click');
    await invokeMethod('settingsdialog_toolbutton_demo@demo.owncloud.com', 'click');
    // close settings window
    //await invokeMethod('Settings', 'accept');
});

// test('desktopNotificationsCheckBox clicked', async t => {
//     await waitForSignal('desktopNotificationsCheckBox', 'stateChanged(int)');
// });

test('list widgets', async t => {
    await listObjects();
});