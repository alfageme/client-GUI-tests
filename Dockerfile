FROM ubuntu:16.10
MAINTAINER Samuel Alfageme <samuel@owncloud.com>

ENV DISPLAY :5
ENV DEBIAN_FRONTEND noninteractive
ENV LD_LIBRARY_PATH /opt/ownCloud/Qt-5.6.2/lib/x86_64-linux-gnu/:$(LD_LIBRARY_PATH)
ENV CMAKE_FLAGS -DQT_QMAKE_EXECUTABLE=/opt/ownCloud/Qt-5.6.2/bin/qmake \
		-DCMAKE_INSTALL_PREFIX=/opt/ownCloud/Qt-5.6.2 \
		-DCMAKE_INSTALL_DOCDIR=/usr/share/doc \
		-DCMAKE_INSTALL_DATAROOTDIR:PATH=/usr/share \
		-DDATA_INSTALL_DIR:PATH=/usr/share \
		-DSYSCONF_INSTALL_DIR=/etc \
		-DWITH_DOC=FALSE \
		-DCMAKE_SKIP_RPATH=FALSE \
		-DCMAKE_INSTALL_RPATH_USE_LINK_PATH=TRUE \
		-DOEM_THEME_DIR=$(CURDIR)/owncloud/syncclient \
		-DCMAKE_FIND_ROOT_PATH=/opt/ownCloud/Qt-5.6.2 \
		-DBUILD_SHELL_INTEGRATION=OFF

RUN apt-get update && apt-get install -y \
        git \
        dbus \
        ca-certificates \
        wget \
        xvfb \
        imagemagick

RUN wget -nv http://download.opensuse.org/repositories/isv:ownCloud:devel:Qt562/xUbuntu_16.10/Release.key -O Release.key
RUN apt-key add - < Release.key

RUN sh -c "echo 'deb-src http://download.opensuse.org/repositories/isv:/ownCloud:/devel:/Qt562/xUbuntu_16.10/ /' > /etc/apt/sources.list.d/owncloud-client.list"
RUN sh -c "echo 'deb http://download.opensuse.org/repositories/isv:/ownCloud:/devel:/Qt562/xUbuntu_16.10/ /'    >> /etc/apt/sources.list.d/owncloud-client.list"

RUN apt-get update && apt-get build-dep -y owncloud-client

RUN git clone --depth 1 https://github.com/dschmidt/owncloud-client.git -b gui-test \
        && cd owncloud-client \
        && git submodule init && git submodule update

# make install is not working, i.e. not simlinking the binaries in the PATH
RUN cd owncloud-client && cmake $CMAKE_FLAGS -DGUI_TESTING=ON && make && make install

# Install nodeJS using nvm
ENV NVM_DIR /usr/local/nvm
ENV NODE_VERSION 7.8.0
RUN rm /bin/sh && ln -s /bin/bash /bin/sh

RUN wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.1/install.sh | bash \
    && source $NVM_DIR/nvm.sh \
    && nvm install $NODE_VERSION \
    && nvm alias default $NODE_VERSION \
    && nvm use default

ENV NODE_PATH $NVM_DIR/v$NODE_VERSION/lib/node_modules
ENV PATH      $NVM_DIR/versions/node/v$NODE_VERSION/bin:$PATH
